.PHONY: all assets

all: assets

assets:
	cd assets; brunch build
	mv priv/static/adminltejs/app.min.js priv/static/js/adminlte.js
