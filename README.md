# PhoenixLTE

AdminLTE HTML Template as a library of Phoenix.View and Phoenix.Template
to be included in new phoenix projects.

## Installation

The package can be installed by adding `phoenix_lte` to your list of dependencies
in `mix.exs`:

```elixir
def deps do
  [
    {:phoenix_lte, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc).

## Development

IMPORTANT: Before commiting a release with changed assets run:

```bash
make assets
git commit ...
```

## Usage

Create a new Phoenix project.

### Static Assets

You can use this package to serve its static assets (css, js, etc.) by using it
in your endpoint:

```elixir
defmodule HelloWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :hello
  
  ...
  use PhoenixLTE.Static, path: "/lte"
  ...
  
end
```

After this, your phoenix app will be serving PhoenixLTE static assets under
`/lte` url.

### Views

In your `lib/project_web/views/layout_view.ex` start using the `Pumba` templates

To use only the stylesheets

```elixir
use PhoenixLTE.Pumba, :style
```

To define and use the beahvor for all the elements like header and sidebar:

```elixir
use PhoenixLTE.Pumba
```

This will define a behaviour on your `LayoutView` that has to define the callbacks.
Here is an example of a defined behaviour:

```elixir
def skin, do: "purple"
def copyright, do: "MTRL UniFI"
def search(_conn), do: true

def logo(conn) do
  %Pumba.Logo{bold: "KMS", normal: "Unifi", link: page_path(conn, :index)}
end

def user(conn) do
  %Pumba.User{name: "Sandro Mehic",
              role: "Developer",
              description: "Since 2015",
              profile_link: page_path(conn, :test, "Profile"),
              sign_out_link: page_path(conn, :test, "Sign out"),
              image_url: "http://image.jpg"}
end

def menu(conn) do
  %Pumba.Menu{header: "Main Menu",
              items: [
                Pumba.MenuItem.child(conn, page_path(conn, :test, "one"), "one", "fa fa-user"),
                Pumba.MenuItem.parent(conn, page_path(conn, :test, "two"), "two", "fa fa-pencil",
                  [Pumba.MenuItem.child(conn, page_path(conn, :test, "three"), "three", "fa fa-eject"),
                    Pumba.MenuItem.child(conn, page_path(conn, :test, "four"), "four", "fa fa-link"),]
                )
              ]
  }
end
```

In your `app.html.eex` layout, include the stylesheets, top and bottom part
of the html from `PhoenixLTE.Pumba`. You can also use `Pumba.content_header`
to set the page header and subheader, that will be passed as assign.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hello Pumba!</title>

    <%= stylesheets(@conn) %>

  </head>

    <%= top(@conn) %>

    <%= Pumba.content_header(%{header: "Hello", subheader: "World"}) %>
    <%= render @view_module, @view_template, assigns %>

    <%= bottom(@conn) %>

</html>
```
