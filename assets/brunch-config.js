exports.config = {
  // See http://brunch.io/#documentation for docs.
  files: {
    javascripts: {
      // joinTo: "js/app.js"

      // To use a separate vendor.js bundle, specify two files path
      // http://brunch.io/docs/config#-files-
      joinTo: {
        "js/app.js": /^app/,
        "js/vendor.js": /^(?!app)/
      }
      //
      // To change the order of concatenation of files, explicitly mention here
      // order: {
      //   before: [
      //     "vendor/js/jquery-2.1.1.js",
      //     "vendor/js/bootstrap.min.js"
      //   ]
      // }
    },
    stylesheets: {
      joinTo: "css/app.css"
    },
    templates: {
      joinTo: "js/app.js"
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/assets/static". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(static)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: ["static", "css", "js", "vendor"],
    // Where to compile files to
    public: "../priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [/vendor/]
    },
    copycat: {
      "css": [
        "node_modules/admin-lte/dist/css/AdminLTE.min.css",
        "node_modules/admin-lte/dist/css/skins/_all-skins.min.css",
        "node_modules/bootstrap/dist/css/bootstrap.min.css",
        "node_modules/bootstrap/dist/css/bootstrap.min.css.map",
        "node_modules/font-awesome/css/font-awesome.css"
      ], // copy these files into priv/static/css/
      "fonts": [
        "node_modules/font-awesome/fonts/fontawesome-webfont.ttf",
        "node_modules/font-awesome/fonts/fontawesome-webfont.woff",
        "node_modules/font-awesome/fonts/fontawesome-webfont.woff2",
        "node_modules/bootstrap/fonts/glyphicons-halflings-regular.ttf",
        "node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff",
        "node_modules/bootstrap/fonts/glyphicons-halflings-regular.woff2"
      ],
      "adminltejs": ["node_modules/admin-lte/dist/js/app.min.js"],
      "js": ["node_modules/bootstrap/dist/js/bootstrap.min.js"],
      verbose: true
    }
  },

  modules: {
    autoRequire: {
      "js/app.js": ["js/app"]
    }
  },

  npm: {
    enabled: true,
    globals: {
      $: 'jquery',
      jQuery: 'jquery'
    }
  }
};
