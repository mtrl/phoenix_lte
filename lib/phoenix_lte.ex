defmodule PhoenixLTE do
  @moduledoc false

  def view do
    quote do
      use Phoenix.View,
        root: "lib/phoenix_lte/templates",
        namespace: PhoenixLTE

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
