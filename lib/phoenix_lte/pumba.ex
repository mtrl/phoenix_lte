defmodule PhoenixLTE.Pumba do
  @moduledoc """
  Pumba serves the templates that can be included in Phoenix projects.
  """
  use PhoenixLTE, :view
  alias __MODULE__

  @callback skin() :: String.t()
  @callback logo(Plug.Conn.t()) :: Pumba.Logo.t()
  @callback user(Plug.Conn.t()) :: Pumba.User.t()
  @callback search(Plug.Conn.t) :: nil | String.t()
  @callback menu(Plug.Conn.t()) :: Pumba.Menu.t()
  @callback copyright() :: String.t()

  @stylesheets ~w[bootstrap.min.css font-awesome.css AdminLTE.min.css _all-skins.min.css]
  @scripts ~w[vendor.js bootstrap.min.js adminlte.js]

  @doc """
  When used, dispatch to the appropriate macro
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
  defmacro __using__(_), do: apply(__MODULE__, :all, [])

  def style do
    quote do
      alias unquote(__MODULE__)

      def stylesheets(conn) do
        %{url: conn.assigns[:phoenix_lte_url]} |> Pumba.stylesheets()
      end

      def scripts(conn) do
        conn.assigns[:phoenix_lte_url] |> Pumba.scripts()
      end
    end
  end

  def all do
    quote do
      alias unquote(__MODULE__)

      @behaviour unquote(__MODULE__)

      def pumba_config(conn) do
        %{
          copyright: copyright(),
          skin: skin(),
          logo: logo(conn),
          user: user(conn),
          search: search(conn),
          menu: menu(conn),
          url: conn.assigns[:phoenix_lte_url],
          conn: conn
        }
      end

      def stylesheets(conn) do
        conn |> pumba_config() |> Pumba.stylesheets()
      end

      def bottom(conn) do
        conn |> pumba_config() |> Pumba.bottom()
      end

      def top(conn) do
        conn |> pumba_config() |> Pumba.top()
      end
    end
  end

  @doc "Include the stylesheets"
  def stylesheets(config) do
    @stylesheets
    |> Enum.map(&element_url(config.url, "/css/", &1))
    |> render_many(__MODULE__, "stylesheet.html", as: :url)
  end

  @doc "Include the scripts"
  def scripts(url) do
    @scripts
    |> Enum.map(&element_url(url, "/js/", &1))
    |> render_many(__MODULE__, "script.html", as: :url)
  end


  @doc "Upper part of the html body"
  def top(config) do
    render(__MODULE__, "top.html", config)
  end

  @doc "Bottom part of the html body"
  def bottom(config) do
    render(__MODULE__, "bottom.html", config)
  end

  @doc """
  Content header API. Use it to generate the content header and subheader
  with AdminLTE css.

  ## Example

      Pumba.content_header(%{header: "example", subheader: "example subheader"})

  """
  def content_header(assigns) do
    render(__MODULE__, "content_header.html", assigns)
  end

  defp content(assigns, key) do
    case Access.fetch(assigns, key) do
      {:ok, value} -> value
      _ -> ""
    end
  end

  defp search_form(assigns) do
    case Access.fetch(assigns, :search) do
      {:ok, nil} -> ""
      {:ok, _} -> render(__MODULE__, "search_form.html", assigns)
      _ -> ""
    end
  end

  defp sidebar_menu_link(item = %Pumba.MenuItem{}) do
    render(__MODULE__, "sidebar_menu_link.html", %{item: item})
  end

  defp sidebar_menu_link({parent, items}) when is_list(items) do
    render(__MODULE__, "sidebar_menu_link_nested.html", %{parent: parent, items: items})
  end

  defp element_url(url, path, element) do
    url <> path <> element
  end
end
