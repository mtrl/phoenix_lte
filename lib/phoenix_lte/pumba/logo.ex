defmodule PhoenixLTE.Pumba.Logo do
  @moduledoc "Struct that holds the text and link for the logo"

  @type t :: %PhoenixLTE.Pumba.Logo{bold: binary, normal: binary, link: binary}

  defstruct [:bold, :normal, :link]
end
