defmodule PhoenixLTE.Pumba.Menu do
  @moduledoc "Struct that holds the menu information"

  @type t :: %PhoenixLTE.Pumba.Menu{header: binary, items: [PhoenixLTE.Pumba.MenuItem.t()]}

  defstruct [:header, :items]
end
