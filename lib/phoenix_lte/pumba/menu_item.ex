defmodule PhoenixLTE.Pumba.MenuItem do
  @moduledoc "Struct that holds menu item information"

  @type t :: %PhoenixLTE.Pumba.MenuItem{class: binary, path: binary, icon: binary, label: binary}

  defstruct [:class, :path, :icon, :label]

  @doc """
  A single MenuItem, can be top level or nested item.
  """
  def child(conn, path, label, icon \\ nil) do
    class = active?(conn.request_path, path, "active")
    %__MODULE__{class: class, path: path, icon: icon, label: label}
  end

  @doc """
  Define a MenuItem that has nested children. Children are already
  created at the moment of the creation of a parent, but no relation
  is saved. Parents class is calculated based on the class of its
  children.
  """
  def parent(_conn, path, label, icon, children) do
    class = active_children?(children, "active")
    parent = %__MODULE__{class: class, path: path, icon: icon, label: label}
    {parent, children}
  end

  defp active?(request_path, path, class) do
    case request_path == path do
      true -> class
      _ -> nil
    end
  end

  defp active_children?(list, class) do
    list
    |> Enum.filter(fn x -> x.class != nil end)
    |> length()
    |> _active(class)
  end

  defp _active(len, class) when len > 0, do: class
  defp _active(_, _), do: nil
end
