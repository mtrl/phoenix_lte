defmodule PhoenixLTE.Pumba.User do
  @moduledoc "Struct that holds the user information"

  @type t :: %PhoenixLTE.Pumba.User{
          name: binary,
          role: binary,
          description: binary,
          image_url: binary,
          profile_link: binary,
          sign_out_link: binary
        }

  defstruct [:name, :role, :description, :image_url, :profile_link, :sign_out_link]
end
