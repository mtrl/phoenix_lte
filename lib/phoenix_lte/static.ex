defmodule PhoenixLTE.Static do
  @moduledoc """
  Static Module can be used within other Phoenix projects in order
  to include serving the static assets, have access to base url, and
  mounted path.

  This will include also a `Plug` in your endpoint, that will save the
  static url and the mounted path for reference inside the `PhoenixLTE`
  components.

  ## Example

      use PhoenixLTE.Static, path: "/lte"

  """
  defmacro __using__(path: path) do
    quote do
      plug(
        Plug.Static,
        at: unquote(path),
        from: :phoenix_lte,
        gzip: false,
        only: ~w(css fonts images js favicon.ico robots.txt)
      )

      plug(:phoenix_lte_url)

      def phoenix_lte_url(conn, _) do
        with url <- apply(__MODULE__, :static_url, []),
             lte_url <- url <> unquote(path) do
          conn
          |> assign(:phoenix_lte_url, lte_url)
          |> assign(:phoenix_lte_base_url, url)
        end
      end
    end
  end
end
