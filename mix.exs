defmodule PhoenixLTE.MixProject do
  use Mix.Project

  def project do
    [
      app: :phoenix_lte,
      version: "0.1.0",
      elixir: "~> 1.6",
      compilers: [:phoenix] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Docs
      name: "PhoenixLTE",
      source_url: "https://gitlab.com/mtrl/phoenix_lte",
      homepage_url: "http://hexdocs.pm/phoenix_lte",
      docs: [main: "readme",
             extras: ["README.md"]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:phoenix, "~> 1.3.0"},
      {:phoenix_html, "~> 2.10"},
      {:ex_doc, "~> 0.16", only: :dev, runtime: false}
    ]
  end
end
